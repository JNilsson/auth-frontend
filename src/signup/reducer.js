import { SIGNUP_REQUEST } from './constants'

const initialState = {
  request: false,
  success: false,
  messages: [],
  errors: []
}

const reducer = function signupReducer (state = initialState, action) {
  switch (action.type) {
    case SIGNUP_REQUEST:
      return {
        request: true,
        success: false,
        messages: [{ body: 'Signing up...', time: new Date() }],
        errors: []
      }

    default:
      return state
  }
}

export default reducer
