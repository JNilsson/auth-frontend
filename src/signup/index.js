import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'

import signupRequest from './actions'

class Signup extends Component {
  render () {
    return (
      <div className='signup'>
        <form className='widget-form'>
          <h1>Signup</h1>
          <label htmlFor='email'>Email</label>
          <Field
            name='email'
            type='text'
            id='email'
            className='email'
            label='Email'
            component='input'
          />
          <label htmlFor='password'>Password</label>
          <Field
            name='password'
            type='password'
            id='password'
            className='password'
            label='Password'
            component='input'
          />
          <button action='submit'>SIGNUP</button>
        </form>
      </div>
    )
  }
}

// connect only elements of state that are needed
const mapStateToProps = state => ({
  signup: state.signup
})

// connect component to redux and attach the `signup` state to `props` in component'
// attach the `signupRequest` action to `props`
const connected = connect(
  mapStateToProps,
  { signupRequest }
)(Signup)

// connect component to reduxForm, and it will auto `namespace` the form `signup`
const formed = reduxForm({
  form: 'signup'
})(connected)

export default formed
