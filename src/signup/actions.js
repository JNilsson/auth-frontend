import { SIGNUP_REQUEST } from './constants'

const signupRequest = function signupRequest ({ email, password }) {
  return {
    type: SIGNUP_REQUEST,
    email,
    password
  }
}

export default signupRequest
